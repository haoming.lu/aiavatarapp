import cv2
import torch
import random
from diffusers import StableDiffusionPipeline, DDIMScheduler, AutoencoderKL
from PIL import Image
from ip_adapter.ip_adapter_faceid import IPAdapterFaceIDPlus
from insightface.app import FaceAnalysis
from insightface.utils import face_align
from modelscope.pipelines import pipeline
from modelscope.utils.constant import Tasks
from modelscope.outputs import OutputKeys
from preprocess import calculate_similarity
from torchvision import io, transforms
import numpy as np
import os
from quality_model import iresnet18


class IPAdapterGenerator:
    def __init__(self, t2i_pipeline, face_quality_model, image_encoder, ipadapter, device=torch.device('cuda')):
        self.device = device
        self.faceanalysis = FaceAnalysis(name="buffalo_l", providers=['CUDAExecutionProvider', 'CPUExecutionProvider'])
        self.faceanalysis.prepare(ctx_id=0, det_size=(640, 640))

        self.face_recognition = pipeline("face_recognition", model='bubbliiiing/cv_retinafce_recognition', model_revision='v1.0.3')
        self.face_quality_model = iresnet18(dropout=0.4, num_features=512, use_se=False).to(self.device)
        self.face_quality_model.load_state_dict(torch.load(face_quality_model, map_location=self.device))
        self.face_quality_model.eval()

        self.quality_transform = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5])
        ])

        vae = AutoencoderKL.from_pretrained('stabilityai/sd-vae-ft-mse').to(dtype=torch.float16)
        noise_scheduler = DDIMScheduler(
            num_train_timesteps=1000,
            beta_start=0.00085,
            beta_end=0.012,
            beta_schedule="scaled_linear",
            clip_sample=False,
            set_alpha_to_one=False,
            steps_offset=1,
        )
        self.t2i_pipeline = StableDiffusionPipeline.from_pretrained(
            t2i_pipeline,
            vae=vae,
            scheduler=noise_scheduler,
            safety_checker=None,
            feature_extractor=None,
            require_safety_checker=False,
            torch_dtype=torch.float16,
        ).to(self.device)
        self.ip_model = IPAdapterFaceIDPlus(self.t2i_pipeline, image_encoder, ipadapter, self.device)

        self.reference_image = None

    def get_face_score(self, face_image):        
        image = Image.open(face_image).resize((112, 112)).convert('RGB')
        image = self.quality_transform(image).to(self.device)
        scores = self.face_quality_model.calculate_serfiq(image.unsqueeze(0), T=10, scaling=5.0)
        return scores[0]
    
    def get_face_similarity(self, face_images):
        embeddings, valid_images = [], []
        for face_image in face_images:
            try:
                embeddings.append(self.face_recognition(dict(user=face_image))[OutputKeys.IMG_EMBEDDING])
            except:
                pass
            else:
                valid_images.append(face_image)
        scores = calculate_similarity(embeddings)
        return scores, valid_images

    def set_reference_images(self, image_paths):
        if not isinstance(image_paths, list):            
            image_paths = [os.path.join(image_paths, filename) for filename in os.listdir(image_paths)]
            image_paths = list(filter(lambda x: x.endswith(('.jpg', '.jpeg', '.png')), image_paths))

        quality_scores = [self.get_face_score(image_path) for image_path in image_paths]
        similarity_scores, image_paths = self.get_face_similarity(image_paths)
        scores = np.array([(x * y).cpu() for x, y in zip(quality_scores, similarity_scores)])
        best_idx = np.argmax(scores)        

        self.reference_image = cv2.imread(image_paths[best_idx])
        faces = self.faceanalysis.get(self.reference_image)
        self.faceid_embeds = torch.from_numpy(faces[0].normed_embedding).unsqueeze(0)
        self.face_image = face_align.norm_crop(self.reference_image, landmark=faces[0].kps, image_size=224)

    def set_reference_image(self, image_path):
        self.reference_image = cv2.imread(image_path)
        faces = self.faceanalysis.get(self.reference_image)
        self.faceid_embeds = torch.from_numpy(faces[0].normed_embedding).unsqueeze(0)
        self.face_image = face_align.norm_crop(self.reference_image, landmark=faces[0].kps, image_size=224)
    
    def generate_with_prompt(
            self, 
            prompts,
            negative_prompts,
            width=512,
            height=512,
            seed=-1,
        ):
        random.seed(seed)

        images = []
        for prompt, negative_prompt in zip(prompts, negative_prompts):
            image = self.ip_model.generate(
                prompt=prompt,
                negative_prompt=negative_prompt,
                face_image=self.face_image,
                faceid_embeds=self.faceid_embeds,
                shortcut=False,
                s_scale=1.0,
                num_samples=1,
                width=width,
                height=height,
                num_inference_steps=30,
                seed=random.randint(0, 0xffffffff),
            )
            images.extend(image)
        return images


prompt = "photo of a woman in red dress in a garden"
negative_prompt = "monochrome, lowres, bad anatomy, worst quality, low quality, blurry"


from os.path import join as pjoin


def benchmark():
    root_dir = '/home/haoming.lu/Avatar_benchmark'
    result_dir = '/home/haoming.lu/AIAvatar/ipadapter_results'
    models = {
        'rv51': 'stablediffusionapi/realistic-vision-51',
        'epic': 'stablediffusionapi/epicrealism',
        'pr13': '/home/haoming.lu/pretrained_models/photorealistic_v1.3',        
    }    

    for model_name, model_path in models.items():
        generator = IPAdapterGenerator(
            t2i_pipeline=model_path,
            face_quality_model='/home/haoming.lu/pretrained_models/ipadapter/resnet18.pth',
            image_encoder='/home/haoming.lu/pretrained_models/ipadapter/image_encoder',
            ipadapter="/home/haoming.lu/pretrained_models/ipadapter/ip-adapter-faceid-plus_sd15.bin",
        )
        for category in ('woman', 'man'):
            raw_prompts = open(pjoin(root_dir, category, 'prompts.txt'), 'r').readlines()
            prompts = {
                'raw': 'sks ' + category,
            }
            for i in range(0, len(raw_prompts), 2):
                prompts[raw_prompts[i][:-1]] = raw_prompts[i + 1][:-1]
            folders = os.listdir(pjoin(root_dir, category))

            for folder in folders:
                if '.txt' in folder:
                    continue
                output_dir = pjoin(result_dir, model_name, category, folder)
                if os.path.exists(output_dir):
                    continue
                generator.set_reference_images(pjoin(root_dir, category, folder))                

                for prompt_name, prompt in prompts.items():
                    os.makedirs(f'{output_dir}/{prompt_name}', exist_ok=True)
                    images = generator.generate_with_prompt(
                        prompts=[prompt] * 8,
                        negative_prompts=[''] * 8,
                    )
                    for iid, image in enumerate(images):
                        image.save(f'{output_dir}/{prompt_name}/{iid}.jpg')


def main():
    generator = IPAdapterGenerator(
        t2i_pipeline='/home/haoming.lu/pretrained_models/photorealistic_v1.3',
        face_quality_model='/home/haoming.lu/pretrained_models/ipadapter/resnet18.pth',
        image_encoder='/home/haoming.lu/pretrained_models/ipadapter/image_encoder',
        ipadapter="/home/haoming.lu/pretrained_models/ipadapter/ip-adapter-faceid-plus_sd15.bin",
    )

    generator.set_reference_images('/home/haoming.lu/Avatar_benchmark/woman/model8')
    images = generator.generate_with_prompt(
        prompts=[prompt] * 4,
        negative_prompts=[negative_prompt] * 4,
    )
    return images


def origin():
    image = cv2.imread("/home/haoming.lu/Avatar_benchmark/woman/model8/model8-8.png")
    app = FaceAnalysis(name="buffalo_l", providers=['CUDAExecutionProvider', 'CPUExecutionProvider'])
    app.prepare(ctx_id=0, det_size=(640, 640))
    faces = app.get(image)

    faceid_embeds = torch.from_numpy(faces[0].normed_embedding).unsqueeze(0)
    face_image = face_align.norm_crop(image, landmark=faces[0].kps, image_size=224) # you can also segment the face

    v2 = False
    base_model_path = "SG161222/Realistic_Vision_V4.0_noVAE"
    vae_model_path = "stabilityai/sd-vae-ft-mse"
    image_encoder_path = "/home/haoming.lu/pretrained_models/ipadapter/image_encoder"
    ip_ckpt = "/home/haoming.lu/pretrained_models/ipadapter/ip-adapter-faceid-plus_sd15.bin"
    device = "cuda"

    noise_scheduler = DDIMScheduler(
        num_train_timesteps=1000,
        beta_start=0.00085,
        beta_end=0.012,
        beta_schedule="scaled_linear",
        clip_sample=False,
        set_alpha_to_one=False,
        steps_offset=1,
    )
    vae = AutoencoderKL.from_pretrained(vae_model_path).to(dtype=torch.float16)
    pipe = StableDiffusionPipeline.from_pretrained(
        base_model_path,
        torch_dtype=torch.float16,
        scheduler=noise_scheduler,
        vae=vae,
        feature_extractor=None,
        safety_checker=None
    )

    # load ip-adapter
    ip_model = IPAdapterFaceIDPlus(pipe, image_encoder_path, ip_ckpt, device)
    images = ip_model.generate(
        prompt=prompt, negative_prompt=negative_prompt, face_image=face_image, faceid_embeds=faceid_embeds, shortcut=v2, s_scale=1.0,
        num_samples=4, width=512, height=768, num_inference_steps=30, seed=2023
    )
    return images

