import torch
import gradio as gr
import aiavatar
from aiavatar.trainer import AvatarTrainer
from aiavatar.dataset import AvatarDataset
import shutil
import os
import diffusers


os.environ["GRADIO_TEMP_DIR"] = './tmp'
model_save_path = '/data/storage/haoml/avatar_models/lora_models/'


def try_load_model(model_name):
    try:
        pipe = diffusers.StableDiffusionPipeline.from_pretrained(model_name)
    except Exception as e:        
        return gr.Text.update(value=str(e))
    else:
        del pipe
        return gr.Text.update(value=f'Model "{model_name}" is valid and ready to use.')


def train(
    model_save_dir,
    files,
    reg_option,
    prior_loss_weight,
    reg_num,
    lora_rank,
    lr_unet,
    lr_ti,
    training_steps,
    pretrained_model,
    method,
    target_class,
):
    instance_path = './training_data/' + model_save_dir
    if os.path.exists(instance_path):
        shutil.rmtree(instance_path)
    os.makedirs(instance_path, exist_ok=True)    
    
    for idx, file in enumerate(files):
        if file.name[-4:].lower() not in ('.png', '.jpg'):
            return 'Only JPG and PNG files are accetped.'        
        shutil.move(file.name, os.path.join(instance_path, '{}.{}'.format(idx, file.name[-3:])))
        
    try:
        if method == 'lorati':
            reg_config, reg_root = aiavatar.resolve_regularization_configs(reg_option)
            training_steps = list(int(x) for x in training_steps.split(','))

            trainer = AvatarTrainer(
                method=method,
                pretrained_model=pretrained_model,
                extra_reg_image_config=reg_config,
                extra_reg_image_root=reg_root,
                prior_loss_weight=prior_loss_weight,
                train_batch_size=1,
                reg_num=int(reg_num),
                training_steps=training_steps,
                lora_rank=int(lora_rank),
                naive_prob=1.0,
                target_class=target_class,
                instance_data_dir=instance_path,
                learning_rate={
                    'unet': lr_unet,
                    'ti': lr_ti,
                },
                output_shape=512,        
            )
        else:
            trainer = AvatarTrainer(
                pretrained_model=pretrained_model,
                method=method,
                class_masked_dataset='/home/haoml/class',
                target_class=target_class,
                instance_data_dir=instance_path,
                learning_rate=float(learning_rate),
                max_train_steps=int(epochs),
                high_quality_upscale=high_quality_upscale,
            )
        trainer.train(model_save_dir=os.path.join(model_save_path, model_save_dir))
    except Exception as e:
        raise e
        message = str(e)
    else:
        message = 'Training completed.'
    
    del trainer
    torch.cuda.empty_cache()

    return gr.Text.update(value=message)


with gr.Blocks() as demo:
    model_root = gr.Text(value=model_save_path, info='The folder to save models.', interactive=True)
    model_save_dir = gr.Text(value='default', label='model_save_dir', info='The name of the fine-tuned model.')
    input_files = gr.File(file_count="multiple")

    with gr.Row():
        reg_option = gr.Dropdown(
            choices=aiavatar.get_regularization_options(),
            label='reg_config',
            info='Select which set of regularization images to use. For option "A-B", A is the source of the images (real means real photos, other means the generative model), B means the category.'
        )
        prior_loss_weight = gr.Number(value=1.0, label='regularization strength', info='The higher this value is, the more likely the fine-tuned model will preserve the generation capacity of the original model.')
        reg_num = gr.Number(value=1, label='ratio between regularization images and instance images', info='When this value is set to 2, a training batch consists of one image of the target object and two regularization images of the same class.')    
    lora_rank = gr.Number(value=32, label='lora rank')
    with gr.Row():
        lr_unet = gr.Number(value=1e-4, label='learning rate of Unet')
        lr_ti = gr.Number(value=5e-4, label='learning rate of the word embedding')
    training_steps = gr.Text(value='800', label='epochs (split by "," for multiple phases). For example "400, 400" means to boost the model with two sub-models, each trained for 400 steps.')
    pretrained_model = gr.Dropdown(
        choices=[
            'stabilityai/stable-diffusion-2-1-base', 
            '/data/storage/haoml/photorealistic/',
            '/home/haoml/photorealistic_v1.3',
            'stablediffusionapi/realistic-vision-51',
            'Lykon/dreamshaper-8',
        ],
        label='pretrained model',
        interactive=True,
        allow_custom_value=True,
        info='To use unlisted external models, directly use the model name from huggingface. For example, you can directly type "stablediffusionapi/realistic-vision-51" from https://huggingface.co/stablediffusionapi/realistic-vision-51.'
    )
    btn_check_model = gr.Button(value='Check (or download) model')    
    target_class = gr.Dropdown(choices=["man", "woman", "cat", "dog"], label="target_class", allow_custom_value=False)
    method = gr.Dropdown(value='lorati', choices=['lorati'], label='method', allow_custom_value=False)
    btn_train = gr.Button(value='Train')
    log_box = gr.Text(interactive=False, label='Message')
    
    btn_check_model.click(try_load_model, inputs=pretrained_model, outputs=log_box)
    btn_train.click(train, inputs=[
        model_save_dir,
        input_files,
        reg_option,
        prior_loss_weight,
        reg_num,
        lora_rank,
        lr_unet,
        lr_ti,
        training_steps,
        pretrained_model,
        method,
        target_class
    ], outputs=log_box)
    

if __name__ == '__main__':
    demo.queue().launch(server_name='0.0.0.0', server_port=4000)
