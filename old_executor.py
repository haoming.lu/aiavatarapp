import torch
import gradio as gr
from aiavatar.executor import AvatarInfer
from aiavatar.dataset import AvatarDataset, ClassDataset
import shutil
import os

os.environ["GRADIO_TEMP_DIR"] = './tmp'

instance_path = 'out/'
batch_size = 4


def get_model_names(model_save_path):
    return gr.Dropdown(choices=list(os.listdir(model_save_path)))


def remove_model(model_save_path, model_name):
    shutil.rmtree(os.path.join(model_save_path, model_name))
    return gr.Textbox(value='Successfully removed ' + os.path.join(model_save_path, model_name))


def Avatar(model_save_path, model_name, backbone_model, prompt, negative_prompt, target_class, num_images, is_lora):    
    # try:
    wrapper = AvatarInfer(
        model_dir=os.path.join(model_save_path, model_name),
        backbone_model_path=backbone_model if is_lora else None,
        is_lora=is_lora,        
    )
    if os.path.exists(instance_path):
        shutil.rmtree(instance_path)
    os.makedirs(instance_path, exist_ok=True)

    filenames = wrapper.eval(
        target_class=target_class,
        prompt=prompt,        
        negative_prompt=negative_prompt,
        num_output_images=int(num_images),
        output_image_dir=instance_path,        
    )
    # except Exception as e:        
    # return gr.Gallery.update(value=None), gr.Textbox.update(value='Error: {}'.format(e))

    return gr.Gallery(value=filenames), gr.Textbox(value='Completed.')


model_names = []

with gr.Blocks() as demo:
    model_save_path = gr.Dropdown(
        choices=['/data/storage/haoml/avatar_models/lora_models/', '/data/storage/haoml/avatar_models/avatar_models_lora/', '/data/storage/haoml/avatar_models/avatar_models'],
        label='Load model from'
    )
    model_name = gr.Dropdown(choices=model_names, label="model_name")
    backbone_model = gr.Dropdown(
        choices=[
            'stabilityai/stable-diffusion-2-1-base', 
            '/data/storage/haoml/photorealistic/',
            # '/home/haoml/pretrained_models/realisticVisionV51',            
            # '/home/haoml/pretrained_models/dreamshaper8',
        ], 
        label='backbone model path'
    )
    is_lora = gr.Checkbox(value=False, label='is LoraTi model')
    btn_getmodels = gr.Button(value="Get trained models")
    btn_getmodels.click(get_model_names, inputs=[model_save_path], outputs=model_name)    

    prompt = gr.Textbox(label="prompt")
    neg_prompt = gr.Textbox(label="negative_prompt")
    target_class = gr.Dropdown(choices=['man', 'woman', 'couple', 'cat', 'dog'], label='Target class')
    num_outputs = gr.Number(label="num_outputs")
    output_images = gr.Gallery()
    running_info = gr.Textbox(label="Log")
    btn_run = gr.Button(value="Run")
    btn_remove = gr.Button(value='Remove selected model')

    btn_remove.click(remove_model, inputs=[model_save_path, model_name], outputs=[running_info])
    btn_run.click(Avatar, inputs=[model_save_path, model_name, backbone_model, prompt, neg_prompt, target_class, num_outputs, is_lora], outputs=[output_images, running_info])
    

if __name__ == "__main__":
    demo.queue().launch(server_name='0.0.0.0', server_port=3124)
