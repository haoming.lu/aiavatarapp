# AI Avatar Demo

Find class images [here](https://drive.google.com/drive/folders/15KvDr5qmbXuDu68rAqz6ohQKL_DNxhNg?usp=share_link)  
You'll need to change the value of `class_masked_dataset` to the path you save the folder above in the trainer.

## Installation
Install the AI Avatar service by `pip install` the latest package file in AIAvatar repo.

## Setup
With a CUDA-enabled environment, run the following commands to start the demo tools.
```
python trainer.py &
python executor.py &
```
You may also want to change the location to save the models or the location to cache uploaded images in the head of the two scripts.  
You can also change the port to host the demo by changing the `launch` function in both scripts.
